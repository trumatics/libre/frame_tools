import pandas as pd
import frame_tools as ft

def test_slice_above():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice_above(df, 'column', limit=5)
    assert sliced_df['column'].min() == 6

def test_slice_above_including_limit():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice_above(df, 'column', limit=5, including_limit=True)
    assert sliced_df['column'].min() == 5

def test_slice_below():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice_below(df, 'column', limit=5)
    assert sliced_df['column'].max() == 4

def test_slice_below_including_limit():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice_below(df, 'column', limit=5, including_limit=True)
    assert sliced_df['column'].max() == 5

def test_slice():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', lower_limit=3, upper_limit=8)
    assert sliced_df['column'].min() == 4
    assert sliced_df['column'].max() == 7

def test_slice_including_limits():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', lower_limit=3, upper_limit=8, including_limits=True)
    assert sliced_df['column'].min() == 3
    assert sliced_df['column'].max() == 8

def test_slice_for_lower_limit():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', lower_limit=3)
    assert sliced_df['column'].min() == 4
    assert sliced_df['column'].max() == 10

def test_slice_for_lower_limit_inclusive():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', lower_limit=3, including_limits=True)
    assert sliced_df['column'].min() == 3
    assert sliced_df['column'].max() == 10


def test_slice_for_upper_limit():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', upper_limit=7)
    assert sliced_df['column'].min() == 1
    assert sliced_df['column'].max() == 6

def test_slice_for_upper_limit_inclusive():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column', upper_limit=7, including_limits=True)
    assert sliced_df['column'].min() == 1
    assert sliced_df['column'].max() == 7

def test_pure_slice():
    df = pd.read_csv('data.csv')
    sliced_df = ft.slice(df, 'column')
    assert sliced_df['column'].min() == 1
    assert sliced_df['column'].max() == 10
